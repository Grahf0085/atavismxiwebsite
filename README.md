# AtavismXI Website

This is the website for the AtavismXI game. <https://www.atavismxi.com>. It provides information pertaining to the game and displays data from the games database.

AtavismXI is the only FFXI private server dedicated to FFXI as it was before TOAU and subsequent expansions.

## Contributions

I could see myself accepting contributions. If it's something minor you can ask for the change in discord. Feel free to make a pull request. I'll review it and get back to you. See existing code for formatting or I can share my prettier options with you.

## Install

1. Clone the project locally `git clone https://gitlab.com/Grahf0085/atavismxiwebsite.git`
2. Change directory into your local copy `cd atavismxiwebsite`
3. Install the dependencies: `npm i`
4. Add a .env file to the root directory (or .env.development / .env.production) with the following keys:
   1. MYSQLHOST=
   2. MYSQLUSER=
   3. MYSQLPASS=
   4. MYSQLDB=
   5. JWT_SECRET_KEY=
   6. TOKEN=
   7. PUBLIC_BASEURL=
5. Provide values for each key.
   1. Items 1-4 should be your credentials for mariaDB that's compatible with ASB/lSB
   2. Generate a random string for item 5 or run `openssl rand -base64 40` to generate a random string
   3. Item 6 should be the name of the token, ie ffxi_token
   4. Item 7 should be http://localhost:2007
6. Run `npm run dev`

## Credits

- [astroJS](https://astro.build/)
- [solidJS](https://www.solidjs.com/)
- [mariaDB nodeJS connector](https://www.npmjs.com/package/mariadb)
- [Lucide Icons](https://lucide.dev/)
- [Felte Forms](https://felte.dev/)
- [JOSE JWT](https://github.com/dvsekhvalnov/jose-jwt)
