import { errors, jwtVerify } from 'jose'
import { defineMiddleware } from 'astro/middleware'

const secret = new TextEncoder().encode(import.meta.env.JWT_SECRET_KEY)
const TOKEN = import.meta.env.TOKEN
const protectedPaths = ['/api/adventurer/delete/', '/api/adventurer/create/']

const isProtected = (route) =>
  protectedPaths.find((path) => (route.startsWith(path) ? true : false))

const verifyAuth = async (token) => {
  if (!token) {
    return {
      status: 'unauthorized',
      msg: 'please pass a request token',
    }
  }

  try {
    const jwtVerifyResult = await jwtVerify(token, secret)

    return {
      status: 'authorized',
      payload: jwtVerifyResult.payload,
      msg: 'successfully verified auth token',
    }
  } catch (err) {
    if (err instanceof errors.JOSEError) {
      return { status: 'error', msg: err.message }
    }

    console.debug(err)
    return { status: 'error', msg: 'could not validate auth token' }
  }
}

export const onRequest = defineMiddleware(async (context, next) => {
  if (!isProtected(context.url.pathname)) {
    return next()
  }

  const token = context.cookies.get(TOKEN)?.value
  const validationResult = await verifyAuth(token)

  switch (validationResult.status) {
    case 'authorized':
      return next()

    case 'error':
    case 'unauthorized':
      if (context.url.pathname.startsWith('/api/')) {
        return new Response(JSON.stringify({ message: validationResult.msg }), {
          status: 401,
        })
      }
      // otherwise, redirect to the root page for the user to login
      else {
        return Response.redirect(new URL('/', context.url))
      }

    default:
      return Response.redirect(new URL('/', context.url))
  }
})
