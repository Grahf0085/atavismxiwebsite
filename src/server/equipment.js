import pool from './pool.js'

export async function findEquipment(name) {
  let connection
  try {
    connection = await pool.getConnection()

    const rows = await connection.query(
      `
SELECT equipslotid, itemid
FROM char_equip
INNER JOIN char_inventory ON char_inventory.charid = char_equip.charid AND char_inventory.slot = char_equip.slotid AND char_inventory.location = char_equip.containerid
INNER JOIN chars ON chars.charid = char_equip.charid
WHERE charname = (?)
`,
      [name],
    )
    return rows
  } catch (error) {
    console.error('Finding Equipment Database Error: ', error)
    throw error
  } finally {
    if (connection) connection.release()
  }
}

export async function findWeaponDetails(weaponId) {
  let connection
  try {
    connection = await pool.getConnection()

    const rows = await connection.query(
      `
SELECT item_weapon.itemId, item_weapon.name, skill, level, jobs, race
FROM item_weapon
INNER JOIN item_equipment ON item_equipment.itemId = item_weapon.itemId
WHERE item_weapon.itemid = (?)
`,
      [weaponId],
    )
    return rows[0]
  } catch (error) {
    console.error('Find Weapon Details Database Error: ', error)
    throw error
  } finally {
    if (connection) connection.release()
  }
}

export async function findArmorDetails(equipmentId) {
  let connection
  try {
    connection = await pool.getConnection()

    const rows = await connection.query(
      `
SELECT item_equipment.itemId, item_basic.name, level, jobs, race
FROM item_equipment
INNER JOIN item_basic ON item_basic.itemid = item_equipment.itemId
WHERE item_equipment.itemid = (?)
`,
      [equipmentId],
    )
    return rows[0]
  } catch (error) {
    console.error('Finding Armor Details Database Error: ', error)
    throw error
  } finally {
    if (connection) connection.release()
  }
}
