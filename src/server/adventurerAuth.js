import pool from './pool.js'

export async function findAdventurer(adventurerName) {
  let connection
  try {
    connection = await pool.getConnection()
    const rows = await connection.query(
      `
SELECT *
FROM chars
WHERE charname = (?)
`,
      [adventurerName],
    )
    return rows
  } catch (error) {
    console.error('Find Player Database Error: ', error)
    throw error
  } finally {
    if (connection) connection.release()
  }
}

export async function createAdventurer(
  accountId,
  adventurerName,
  posZone,
  nation,
  face,
  race,
  size,
  mainJob,
) {
  let connection
  try {
    connection = await pool.getConnection()
    await connection.beginTransaction()

    const charResult = await connection.query(
      `
INSERT INTO chars(charid, accid, charname, pos_zone, nation)
SELECT
  CASE WHEN MAX(charid) IS NULL THEN 1 ELSE MAX(charid) + 1 END, (?), (?), (?), (?)
FROM chars
RETURNING charid
`,
      [accountId, adventurerName, posZone, nation],
    )

    const charId = charResult[0].charid

    await connection.query(
      `
INSERT INTO char_look(charid, face, race, size)
VALUES (?, ?, ?, ?)
`,
      [charId, face, race, size],
    )

    await connection.query(
      `
    INSERT INTO char_stats(charid, mjob)
    VALUES (?, ?)
`,
      [charId, mainJob],
    )

    await connection.query(
      `
    INSERT INTO char_exp(charid)
    VALUES (?)
    ON DUPLICATE KEY UPDATE charid = charid
`,
      [charId],
    )

    await connection.query(
      `
    INSERT INTO char_jobs(charid)
    VALUES (?)
    ON DUPLICATE KEY UPDATE charid = charid
`,
      [charId],
    )
    await connection.query(
      `
    INSERT INTO char_points(charid)
    VALUES (?)
    ON DUPLICATE KEY UPDATE charid = charid
`,
      [charId],
    )
    await connection.query(
      `
    INSERT INTO char_unlocks(charid)
    VALUES (?)
    ON DUPLICATE KEY UPDATE charid = charid
`,
      [charId],
    )
    await connection.query(
      `
    INSERT INTO char_profile(charid)
    VALUES (?)
    ON DUPLICATE KEY UPDATE charid = charid
`,
      [charId],
    )
    await connection.query(
      `
    INSERT INTO char_storage(charid)
    VALUES (?)
    ON DUPLICATE KEY UPDATE charid = charid
`,
      [charId],
    )

    await connection.query(
      `
    DELETE FROM char_inventory
    WHERE charid = (?)
`,
      [charId],
    )

    await connection.query(
      `
    INSERT INTO char_inventory(charid)
    VALUES (?)
`,
      [charId],
    )
    await connection.commit()
  } catch (error) {
    console.error('Create Adventurer Database Error: ', error)
    await connection.rollback()
    throw error.sqlMessage
  } finally {
    if (connection) connection.release()
  }
}

export async function deleteAdventurer(accid, charid) {
  let connection
  try {
    connection = await pool.getConnection()
    await connection.query(
      `
UPDATE chars
SET accid = 0,
original_accid = (?),
charname = CONCAT('_', charname)
WHERE charid = (?) AND accid = (?)
`,
      [accid, charid, accid],
    )
  } catch (error) {
    console.error('Delete Adventurer Database Error:', error)
    throw error.sqlMessage
  } finally {
    if (connection) connection.release()
  }
}
