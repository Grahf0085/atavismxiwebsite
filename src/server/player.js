import pool from './pool.js'

export async function findPlayersAdventurers(playerId) {
  let connection
  try {
    connection = await pool.getConnection()
    const rows = await connection.query(
      `
SELECT charname, mjob, sjob, mlvl, slvl, face, race, pos_zone, nation
      FROM chars
      INNER JOIN char_stats ON char_stats.charid = chars.charid
      LEFT JOIN accounts_sessions ON accounts_sessions.charid = chars.charid
      INNER JOIN char_look ON char_look.charid = chars.charid
			WHERE chars.accid = (?)
`,
      [playerId],
    )
    return rows
  } catch (error) {
    console.error('Find By Player Adventurers Database Error:', error)
    throw error
  } finally {
    if (connection) connection.release()
  }
}
