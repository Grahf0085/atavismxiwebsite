import pool from './pool.js'

export async function findAllOnline() {
  let connection
  try {
    connection = await pool.getConnection()

    const rows = await connection.query(`
      SELECT charname, mjob, sjob, mlvl, slvl, face, race, pos_zone, nation
      FROM chars
      INNER JOIN char_stats ON char_stats.charid = chars.charid
      INNER JOIN accounts_sessions ON accounts_sessions.charid = chars.charid
      INNER JOIN char_look ON char_look.charid = chars.charid
    `)
    return rows
  } catch (error) {
    console.error('Find All Online Database Error: ', error)
    throw error.sqlMessage
  } finally {
    if (connection) connection.release()
  }
}

export async function findByName(name) {
  let connection
  try {
    connection = await pool.getConnection()

    const rows = await connection.query(
      `
      SELECT chars.accid, chars.charid, charname, mjob, sjob, mlvl, slvl, face, race, pos_zone, nation, title, war, mnk, whm, blm, rdm, thf, pld, drk, bst, brd, rng, sam, 
      nin, drg, smn, rank_bastok AS rankBastok, rank_windurst AS rankWindurst, rank_sandoria AS rankSandoria,
      (SELECT value FROM char_skills WHERE skillid = 48 AND char_skills.charid = chars.charid) AS fishing,
      (SELECT value FROM char_skills WHERE skillid = 49 AND char_skills.charid = chars.charid) AS woodworking,
      (SELECT value FROM char_skills WHERE skillid = 50 AND char_skills.charid = chars.charid) AS smithing,
      (SELECT value FROM char_skills WHERE skillid = 51 AND char_skills.charid = chars.charid) AS goldsmithing,
      (SELECT value FROM char_skills WHERE skillid = 52 AND char_skills.charid = chars.charid) AS weaving,
      (SELECT value FROM char_skills WHERE skillid = 53 AND char_skills.charid = chars.charid) AS leathercraft,
      (SELECT value FROM char_skills WHERE skillid = 54 AND char_skills.charid = chars.charid) AS bonecraft,
      (SELECT value FROM char_skills WHERE skillid = 55 AND char_skills.charid = chars.charid) AS alchemy,
      (SELECT value FROM char_skills WHERE skillid = 56 AND char_skills.charid = chars.charid) AS cooking,
      (SELECT value FROM char_skills WHERE skillid = 59 AND char_skills.charid = chars.charid) AS digging,
      IF(accounts_sessions.charid IS NULL, 0, 1) AS 'online'
      FROM chars
      INNER JOIN char_stats ON char_stats.charid = chars.charid
      INNER JOIN char_jobs ON char_jobs.charid = chars.charid
      INNER JOIN char_profile ON char_profile.charid = chars.charid
      INNER JOIN char_look ON char_look.charid = chars.charid
      LEFT JOIN accounts_sessions ON accounts_sessions.charid = chars.charid
      WHERE chars.charname LIKE (?) AND chars.accid != 0
`,
      [`%${name}%`],
    )
    return rows
  } catch (error) {
    console.error('Find By Name Database Error: ', error)
    throw error.sqlMessage
  } finally {
    if (connection) connection.release()
  }
}
