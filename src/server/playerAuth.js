import pool from './pool.js'

export async function findPlayer({
  where: { playerName = undefined, id = undefined },
}) {
  let connection
  try {
    if (id !== undefined && !isNaN(id)) {
      connection = await pool.getConnection()

      const rows = await connection.query(
        `
SELECT * FROM accounts 
WHERE id = (?)
`,
        [id],
      )
      return rows[0]
    } else if (playerName !== undefined) {
      connection = await pool.getConnection()

      const rows = await connection.query(
        `
      SELECT * FROM accounts 
      WHERE login = ?
      `,
        [playerName],
      )
      return rows[0]
    }
    return null
  } catch (error) {
    console.error('Find Player Database Error: ', error)
    throw error.message
  } finally {
    if (connection) connection.release()
  }
}

export async function createPlayer(username, password) {
  let connection
  try {
    connection = await pool.getConnection()

    const rows = await connection.query(
      `
INSERT INTO accounts(id, login, password, current_email, registration_email, timecreate, timelastmodify, content_ids, expansions, features, status, priv) 
SELECT
  CASE WHEN MAX(id) IS NULL THEN 1000 ELSE MAX(id) + 1 END,
  (?), 
  Password((?)),
  '',
  '',
  NOW(),
  NOW(),
  3,
  6,
  0,
  1,
  1
FROM accounts 
RETURNING id, login
`,
      [username, password],
    )
    return rows[0]
  } catch (error) {
    console.error('Create Player Database Error: ', error)
    throw error.message
  } finally {
    if (connection) connection.release()
  }
}

export async function login(playerName, password) {
  let connection
  try {
    connection = await pool.getConnection()

    const player = await findPlayer({ where: { playerName } })
    if (!player) return null

    const dbPassword = player.password
    const enteredAndHashedPassword = await connection.query(
      `
SELECT PASSWORD((?)) AS hashedPassword
`,
      [password],
    )
    const correctPassword =
      dbPassword === enteredAndHashedPassword[0].hashedPassword

    if (!correctPassword) return null

    return player
  } catch (error) {
    console.error('Logging In Database Error: ', error)
    throw error.message
  } finally {
    if (connection) connection.release()
  }
}
