import pool from './pool.js'

export async function findRecipesByCraft(craft) {
  let connection
  try {
    connection = await pool.getConnection()

    const rows = await connection.query(
      `SELECT ID AS id, Wood AS wood, Smith AS smith, Gold AS gold, Cloth as cloth, Leather AS leather, Bone AS bone, Alchemy AS alchemy, Cook AS cook,
      (SELECT name from item_basic WHERE item_basic.itemid = synth_recipes.crystal) AS crystal,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Ingredient1) AS ingredientOne,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Ingredient2) AS ingredientTwo,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Ingredient3) AS ingredientThree,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Ingredient4) AS ingredientFour,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Ingredient5) AS ingredientFive,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Ingredient6) AS ingredientSix,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Ingredient7) AS ingredientSeven,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Ingredient8) AS ingredientEight,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.Result) AS result,
      ResultQty AS resultQty,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.ResultHQ1) AS resultHqOne,
      ResultHQ1Qty AS resultHqOneQty,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.ResultHQ2) AS resultHqTwo,
      ResultHQ2Qty AS resultHqTwoQty,
      (SELECT name FROM item_basic WHERE item_basic.itemid = synth_recipes.ResultHQ3) AS resultHqThree,
      ResultHQ3Qty AS resultHqThreeQty
      FROM synth_recipes
      WHERE (
        CASE (?)
          WHEN 'wood'
            THEN synth_recipes.Wood > 0
          WHEN 'smith'
            THEN synth_recipes.Smith > 0
          WHEN 'gold'
            THEN synth_recipes.Gold > 0
          WHEN 'cloth'
            THEN synth_recipes.Cloth > 0
          WHEN 'leather'
            THEN synth_recipes.Leather > 0
          WHEN 'bone'
            THEN synth_recipes.Bone > 0
          WHEN 'alchemy'
            THEN synth_recipes.Alchemy > 0
          WHEN 'cook'
            THEN synth_recipes.Cook > 0
        END
      )
      ORDER BY (
        CASE (?)
          WHEN 'wood'
            THEN synth_recipes.Wood
          WHEN 'smith'
            THEN synth_recipes.Smith
          WHEN 'gold'
            THEN synth_recipes.Gold
          WHEN 'cloth'
            THEN synth_recipes.Cloth
          WHEN 'leather'
            THEN synth_recipes.Leather
          WHEN 'bone'
            THEN synth_recipes.Bone
          WHEN 'alchemy'
            THEN synth_recipes.Alchemy
          WHEN 'cook'
            THEN synth_recipes.Cook
      END
)
`,
      [craft, craft],
    )
    return rows
  } catch (error) {
    console.error('Find Reciples Database Error:', error)
    throw error
  } finally {
    if (connection) connection.release()
  }
}
