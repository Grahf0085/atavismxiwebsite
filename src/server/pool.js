import mariadb from 'mariadb'

const pool = mariadb.createPool({
  host: import.meta.env.MYSQLHOST,
  user: import.meta.env.MYSQLUSER,
  password: import.meta.env.MYSQLPASS,
  database: import.meta.env.MYSQLDB,
  port: import.meta.env.MYSQLPORT ? parseInt(import.meta.env.MYSQLPORT) : 3306,
  waitForConnections: true,
  connectionLimit: 20,
})

export default pool
