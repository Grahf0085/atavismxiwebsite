import hume from '../media/races/hume.png'
import elvaan from '../media/races/elvaan.png'
import taru from '../media/races/taru.png'
import galka from '../media/races/galka.png'
import mithra from '../media/races/mithra.png'

export const raceProfiles = [
  {
    race: 'hume',
    pic: hume.src,
    alt: 'A Hume male and female',
    description: `Humes are characterized by their versatile and balanced attributes,
            making them well-suited for various jobs and roles within the diverse
            world of Vana'diel. Physically, Humes possess a generally humanoid
            appearance, with a variety of skin tones, eye colors, and hairstyles.
            They are known for their adaptability, making them proficient in a
            wide range of classes, from skilled warriors to powerful mages. Humes
            hail from the Republic of Bastok. Bastok is an industrialized
            city-state with a focus on technology and commerce, and Humes from
            this region often carry traits of determination and industriousness.
            In the ever-expanding world of Vana'diel, Humes can be found engaging
            in all aspects of life, whether as adventurers exploring dangerous
            dungeons, skilled artisans crafting powerful equipment, or scholars
            unraveling the mysteries of magic. Their ability to excel in various
            professions makes them an essential and adaptable presence in the rich
            tapestry of Final Fantasy XI's diverse and dynamic population.`,
  },
  {
    race: 'elvaan',
    pic: elvaan.src,
    alt: 'A Elvaan male and female',
    description: `Elvaan are an elegant and regal race, known for their tall stature,
            noble bearing, and refined features. Elvaan characters exhibit a
            graceful and majestic presence, standing taller than other races in
            Vana'diel. Physically, Elvaan possess pointed ears and distinctive
            facial features, giving them an otherworldly and ethereal appearance.
            Elvaan hair is typically long and flowing, with a variety of styles to
            choose from during character creation. The Elvaan homeland is the
            Kingdom of San d'Oria. Elvaan characters from San d'Oria often exhibit
            qualities of honor, duty, and a deep connection to their kingdom's
            history and values. In terms of gameplay, Elvaan characters are
            well-suited for melee classes, such as warriors, paladins, and
            dragoons, owing to their physical prowess and impressive strength.
            Their cultural emphasis on knightly virtues aligns with the defensive
            and martial aspects of these jobs. In the vast world of Vana'diel,
            Elvaan are both skilled warriors and knowledgeable scholars,
            contributing to the diverse tapestry of adventurers striving to
            maintain balance and peace in a realm threatened by various forces.
            Their elegant and noble presence adds a touch of sophistication to the
            rich and dynamic landscape of Final Fantasy XI.`,
  },
  {
    race: 'taru',
    pic: taru.src,
    alt: 'A Taru male and female',
    description: `Tarutaru are a charming and diminutive race known for their small
            stature, childlike appearance, and innate magical aptitude. Despite
            their size, Tarutaru possess a unique and whimsical charm that belies
            their potent magical abilities. Physically, Tarutaru are characterized
            by their small frames, large heads, and expressive faces. They have
            round, big eyes that convey a sense of innocence and curiosity.
            Tarutaru skin tones range from pale to various shades of tan, and
            their hair comes in a variety of vibrant colors. Their childlike
            appearance can be misleading, as Tarutaru are adept spellcasters and
            scholars. The Tarutaru homeland is the Federation of Windurst. In the
            bustling streets of Windurst, one can find Tarutaru engaging in
            magical experiments, studying ancient tomes, or practicing their craft
            in the magical arts. In terms of gameplay, Tarutaru excel as
            spellcasters, particularly in mage classes such as Black Mage, White
            Mage, and Red Mage. Their small size and playful demeanor can be
            deceiving, as they wield powerful and destructive magic with
            precision. In the vast and enchanting world of Vana'diel, Tarutaru
            contribute their unique blend of magical prowess and endearing charm,
            making them an essential part of the diverse community of adventurers
            striving to unravel the mysteries and challenges that the realm
            presents.`,
  },
  {
    race: 'galka',
    pic: galka.src,
    alt: 'A Galka',
    description: `Galka are a formidable and imposing race characterized by their
            massive, muscular builds and beast-like appearances. Hailing from the
            barren lands of the Quon continent, Galka are easily distinguished by
            their hulking frames, unique features, and a mysterious, solitary
            nature. Physically, Galka are robust and heavily built, with
            animalistic traits such as large, pointed ears and sharp, angular
            facial features. Their skin tones range from earthy browns to deep
            grays, reflecting their rugged origins in the harsh deserts of their
            homeland. Galka possess a powerful and intimidating presence, and
            their voices resonate with a deep and primal timbre. Galka have a
            nomadic and solitary culture, with a strong emphasis on individual
            strength and self-reliance. They are known for their resilience and
            endurance, qualities that serve them well in the challenging
            environments they often find themselves in. The Galka homeland is the
            Republic of Bastok, where they coexist with the Humes, forging their
            own path in the industrialized city-state. In terms of gameplay, Galka
            excel in physical combat and are often chosen for jobs such as
            warriors, monks, and thieves. Their impressive strength and durability
            make them formidable tanks on the battlefield, capable of withstanding
            heavy damage. In the expansive world of Vana'diel, Galka contribute
            their imposing presence and physical might to the diverse community of
            adventurers. With a culture shaped by survival in harsh conditions,
            Galka bring a sense of tenacity and individualism to the ever-evolving
            tapestry of Final Fantasy XI.`,
  },
  {
    race: 'mithra',
    pic: mithra.src,
    alt: 'A Mithra',
    description: `Mithra are a feline-like race known for their agility, grace, and
            enigmatic charm. Hailing from the southern seas, the Mithra possess a
            distinct combination of feline features and humanoid characteristics
            that set them apart in the diverse world of Vana'diel. Physically,
            Mithra have cat-like ears, tails, and feline-like facial features.
            Their eyes are often large and expressive, with vertical slit pupils
            that give them an alluring and mysterious appearance. Mithra come in a
            variety of fur patterns and colors, and their lithe, athletic bodies
            reflect their natural agility. Mithra society is matriarchal, with
            female Mithra taking on leadership roles and guiding their tribes. The
            Mithra homeland is the land of Kazham, situated on a secluded island
            in the southern seas. Kazham is a tropical paradise, and the Mithra
            embrace the freedom of the open seas, often engaging in trade and
            exploration. In terms of gameplay, Mithra are well-suited for agile
            and dexterous jobs such as thieves and ninjas. Their natural agility
            and heightened senses make them adept at evading attacks and striking
            quickly in combat. In the vibrant and diverse world of Vana'diel,
            Mithra contribute their unique blend of feline grace and mysterious
            charm to the community of adventurers. Whether exploring the vast
            landscapes, engaging in trade, or delving into ancient mysteries, the
            Mithra add a touch of agility and allure to the rich tapestry of Final
            Fantasy XI.`,
  },
]
