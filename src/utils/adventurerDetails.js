import {
  jobConverter,
  formatAdventurer,
  adventurerImage,
  zones,
} from './adventurerConversion'

import bastokEmblem from '../media/nations/bastok.webp'
import sandoriaEmblem from '../media/nations/sandoria.webp'
import windurstEmblem from '../media/nations/windurst.webp'

import { titles } from './titles.js'

export const nationEmblems = [
  { pic: sandoriaEmblem, alt: 'Emblem of Sandoria' },
  { pic: bastokEmblem, alt: 'Emblem of Bastok' },
  { pic: windurstEmblem, alt: 'Emblem of Windurst' },
]

const baseUrl = import.meta.env.PUBLIC_BASEURL

export const fetchAdventurerDetails = async (name) => {
  try {
    const response = await fetch(`${baseUrl}/api/adventurer/${name}`)

    if (!response.ok) {
      throw await response.json()
    }
    const allAdventurerDetails = await response.json()

    const adventurerDetails = allAdventurerDetails.find(
      (adventurer) => adventurer.charname === name,
    )

    const accountId = adventurerDetails?.accid
    const charId = adventurerDetails?.charid
    const charName = adventurerDetails?.charname
    const mainLevel = adventurerDetails?.mlvl
    const subLevel = adventurerDetails?.slvl
    const mainJob = jobConverter(adventurerDetails?.mjob)
    const subJob = jobConverter(adventurerDetails?.sjob)

    const formattedAdventurer = formatAdventurer(
      adventurerDetails?.race,
      adventurerDetails?.face,
    )

    const fetchedImage = adventurerImage(formattedAdventurer)
    const title = titles[adventurerDetails?.title]
    const translatedZone = zones[adventurerDetails?.pos_zone]

    const nations = [
      {
        rank: adventurerDetails?.rankSandoria,
        pic: nationEmblems[0].pic.src,
        alt: nationEmblems[0].alt,
      },
      {
        rank: adventurerDetails?.rankBastok,
        pic: nationEmblems[1].pic.src,
        alt: nationEmblems[1].alt,
      },
      {
        rank: adventurerDetails?.rankWindurst,
        pic: nationEmblems[2].pic.src,
        alt: nationEmblems[2].alt,
      },
    ]

    const jobs = [
      {
        job: 'WAR',
        level: adventurerDetails?.war,
      },
      {
        job: 'MNK',
        level: adventurerDetails?.mnk,
      },
      {
        job: 'WHM',
        level: adventurerDetails?.whm,
      },
      {
        job: 'BLM',
        level: adventurerDetails?.blm,
      },
      {
        job: 'RDM',
        level: adventurerDetails?.rdm,
      },
      {
        job: 'THF',
        level: adventurerDetails?.thf,
      },
      {
        job: 'PLD',
        level: adventurerDetails?.pld,
      },
      {
        job: 'DRK',
        level: adventurerDetails?.drk,
      },
      {
        job: 'BST',
        level: adventurerDetails?.bst,
      },
      {
        job: 'BRD',
        level: adventurerDetails?.brd,
      },
      {
        job: 'RNG',
        level: adventurerDetails?.rng,
      },
      {
        job: 'SAM',
        level: adventurerDetails?.sam,
      },
      {
        job: 'NIN',
        level: adventurerDetails?.nin,
      },
      {
        job: 'DRG',
        level: adventurerDetails?.drg,
      },
      {
        job: 'SMN',
        level: adventurerDetails?.smn,
      },
    ]

    const crafts = [
      {
        title: 'Fishing',
        level: adventurerDetails?.fishing || 0,
      },
      {
        title: 'Woodworking',
        level: adventurerDetails?.woodworking || 0,
      },
      {
        title: 'Smithing',
        level: adventurerDetails?.smithing || 0,
      },
      {
        title: 'Goldsmithing',
        level: adventurerDetails?.goldsmithing || 0,
      },
      {
        title: 'Clothcraft',
        level: adventurerDetails?.weaving || 0,
      },
      {
        title: 'Leathercraft',
        level: adventurerDetails?.leathercraft || 0,
      },
      {
        title: 'Bonecraft',
        level: adventurerDetails?.bonecraft || 0,
      },
      {
        title: 'Alchemy',
        level: adventurerDetails?.alchemy || 0,
      },
      {
        title: 'Cooking',
        level: adventurerDetails?.cooking || 0,
      },
      {
        title: 'Digging',
        level: adventurerDetails?.digging || 0,
      },
    ]

    return {
      accountId,
      charId,
      charName,
      mainLevel,
      subLevel,
      mainJob,
      subJob,
      fetchedImage,
      title,
      nations,
      jobs,
      crafts,
      translatedZone,
    }
  } catch (error) {
    console.error('Error Fetching Adventurer Details: ', error)
    return {
      accountId: '',
      charId: '',
      charName: '',
      mainLevel: '',
      subLevel: '',
      mainJob: '',
      subJob: '',
      fetchedImage: '',
      title: '',
      nations: '',
      jobs: '',
      crafts: '',
      translatedZone: '',
    }
  }
}
