import { jwtVerify, SignJWT } from 'jose'
import { nanoid } from 'nanoid'
import { findPlayer } from '../server/playerAuth'

const TOKEN = import.meta.env.TOKEN
const secret = new TextEncoder().encode(import.meta.env.JWT_SECRET_KEY)

export const getPlayerSession = (cookies) => {
  return cookies.get(TOKEN)?.value
}

export async function getPlayerId(cookies) {
  const session = getPlayerSession(cookies)
  if (session) {
    const jwtVerifyResult = await jwtVerify(session, secret)
    const { playerId } = jwtVerifyResult.payload
    if (!playerId || typeof playerId !== 'number') return null
    return playerId
  }
  return null
}

export async function getPlayer(cookies) {
  const playerId = await getPlayerId(cookies)

  if (typeof playerId !== 'number') {
    return null
  }
  try {
    const player = await findPlayer({ where: { id: playerId } })
    return player
  } catch (error) {
    console.error('Error Getting Player: ', error)
    throw error
  }
}

export async function createToken(context, playerId) {
  const token = await new SignJWT({ playerId: playerId })
    .setProtectedHeader({ alg: 'HS256' })
    .setJti(nanoid())
    .setIssuedAt()
    .setExpirationTime('48h')
    .sign(secret)

  context.cookies.set(TOKEN, token, {
    httpOnly: true,
    path: '/',
    maxAge: 60 * 60 * 48, // 48 hours in seconds
    secure: true,
    sameSite: 'none',
    domain: '.atavismxi.com',
  })

  return token
}
