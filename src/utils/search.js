const baseUrl = import.meta.env.PUBLIC_BASEURL

export const fetchOnline = async (searchProps) => {
  const selectedZone = searchProps[0]
  const selectedJob = searchProps[1]

  let filteredByZone
  let filteredByJob

  try {
    const response = await fetch(`${baseUrl}/api/adventurer/online`)

    if (!response.ok) {
      throw await response.json()
    }

    const adventurersOnline = await response.json()

    if (selectedZone !== null && selectedZone !== undefined) {
      filteredByZone = adventurersOnline.filter(
        (adventurer) => adventurer.pos_zone === selectedZone,
      )
      filteredByJob = filteredByJob?.filter(
        (adventurer) => adventurer.pos_zone === selectedZone,
      )
    }

    if (selectedJob !== null && selectedJob !== undefined) {
      filteredByJob = adventurersOnline.filter(
        (adventurer) => adventurer.mjob === selectedJob,
      )
      filteredByZone = filteredByZone?.filter(
        (adventurer) => adventurer.mjob === selectedJob,
      )
    }

    const onlineZones = [
      ...new Set(adventurersOnline.map((adventurer) => adventurer.pos_zone)),
    ]

    const onlineJobs = [
      ...new Set(adventurersOnline.map((adventurer) => adventurer.mjob)),
    ]

    const numberInEachArea = adventurersOnline.reduce((acc, obj) => {
      const zoneNumber = obj.pos_zone
      acc[zoneNumber] = (acc[zoneNumber] || 0) + 1
      return acc
    }, {})

    const jobsInEachArea = adventurersOnline.reduce((acc, obj) => {
      const jobNumber = obj.mjob
      acc[jobNumber] = (acc[jobNumber] || 0) + 1
      return acc
    }, {})

    const errors = undefined

    return {
      adventurersOnline,
      onlineZones,
      onlineJobs,
      numberInEachArea,
      jobsInEachArea,
      filteredByZone,
      filteredByJob,
      errors,
    }
  } catch (error) {
    console.error('Error Fetching Adventurers Online: ', error)
    const errors = error
    return {
      adventurersOnline: [],
      onlineZones: [],
      onlineJobs: [],
      numberInEachArea: {},
      jobsInEachArea: {},
      errors,
    }
  }
}

export const fetchRecipes = async (craft) => {
  try {
    const response = await fetch(`${baseUrl}/api/craft/${craft}`)

    if (!response.ok) {
      throw await response.json()
    }

    const data = await response.json()
    return data
  } catch (error) {
    console.error('Error Fetching Recipes: ', error)
    return error.errors.sqlMessage
  }
}
