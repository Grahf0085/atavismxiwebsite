export const getHeaders = (origin) => {
  if (
    origin === 'tauri://localhost' ||
    origin === 'https://tauri.localhost' ||
    origin === 'http://localhost.atavismxi.com:4321' ||
    origin === 'http://localhost:1420' ||
    origin === 'https://atavismxi.com'
  ) {
    return {
      'Access-Control-Allow-Origin': origin,
      'Content-Type': 'application/json',
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Credentials': 'true',
    }
  }
}
