import {
  For,
  createSignal,
  createResource,
  Show,
  onCleanup,
  startTransition,
  onMount,
  Suspense,
} from 'solid-js'
import { Select } from '@thisbeyond/solid-select'
import { createForm } from '@felte/solid'
import Adventurer from './Adventurer'
import { jobConverter, zones } from '../../utils/adventurerConversion'
import { fetchOnline } from '../../utils/search'
import '@thisbeyond/solid-select/style.css'
import '../../styles/adventurer/adventurers.css'

const baseUrl = import.meta.env.PUBLIC_BASEURL

export default function Adventurers() {
  const [selectedZone, setSelectedZone] = createSignal(null)
  const [selectedJob, setSelectedJob] = createSignal()
  const [fetchedByName, setFetchedByName] = createSignal()
  const [initialSelectValue, setInitialSelectValue] = createSignal(null, {
    equals: false,
  })

  const [fetchedOnline, { refetch }] = createResource(
    () => [selectedZone(), selectedJob()],
    fetchOnline,
  )

  onMount(() => {
    const timer = setInterval(() => startTransition(refetch), 3000)

    onCleanup(() => clearInterval(timer))
  })

  const { form, errors } = createForm({
    onSubmit: async (values) => {
      if (values.nameSearch === '') return []

      const searchResponse = await fetch(
        `${baseUrl}/api/adventurer/${values.nameSearch}`,
      )

      if (!searchResponse.ok) {
        throw await searchResponse.json()
      }

      const searchResults = await searchResponse.json()

      return searchResults
    },

    validate: (values) => {
      const errors = {}

      return errors
    },

    onSuccess(response) {
      setFetchedByName(response)
    },

    onError(err) {
      const errors = {}
      errors.serverError = err.errors
      return errors
    },
  })

  return (
    <Suspense>
      <div class='adventurersContainer'>
        <form class='adventurerForm' use:form>
          <label class='adventurerLabel'>
            Search By Name
            <input
              class='adventurerInput'
              type='text'
              placeholder='Find An Adventurer'
              name='nameSearch'
            />
          </label>
          <Show
            when={
              fetchedByName() === undefined || fetchedByName()?.length === 0
            }
          >
            <div class='zoneSelectContainer'>
              <label class='adventurerLabel' for='zoneSelect'>
                Filter Online By Zone
              </label>
              <Select
                initialValue={initialSelectValue()}
                onChange={(e) => setSelectedZone(e)}
                class='custom'
                options={fetchedOnline()?.onlineZones}
                format={(item) =>
                  zones[item] +
                  '(' +
                  fetchedOnline()?.numberInEachArea[item] +
                  ')'
                }
                placeholder='Search A Zone'
                id='zoneSelect'
              />
            </div>
            <div class='jobSelectContainer'>
              <label class='adventurerLabel' for='jobSelect'>
                Filter Online By Job
              </label>
              <Select
                initialValue={initialSelectValue()}
                onChange={(e) => setSelectedJob(e)}
                class='custom'
                options={fetchedOnline()?.onlineJobs}
                format={(item) =>
                  jobConverter(item) +
                  '(' +
                  fetchedOnline()?.jobsInEachArea[item] +
                  ')'
                }
                placeholder='Search A Job'
                id='jobSelect'
              />
            </div>
          </Show>
          <div class='buttonContainer'>
            <button
              type='reset'
              onClick={() => {
                setSelectedZone(null)
                setSelectedJob(undefined)
                setFetchedByName(undefined)
                setInitialSelectValue(null)
              }}
            >
              Reset
            </button>
            <button type='submit'>Search</button>
            <Show
              when={errors().serverError || fetchedOnline()?.errors?.errors}
            >
              <p>
                {errors().serverError} {fetchedOnline()?.errors?.errors}
              </p>
            </Show>
          </div>
        </form>
        <div class='resultsContainer'>
          <Show
            when={
              fetchedByName() === undefined || fetchedByName()?.length === 0
            }
            fallback={
              <section>
                <h2 class='onlineTitle'>Search Results</h2>
                <ul class='adventurerListContainer'>
                  <For each={fetchedByName()}>
                    {(adventurer) => (
                      <li class='adventurerRow'>
                        <Adventurer {...adventurer} />
                      </li>
                    )}
                  </For>
                </ul>
              </section>
            }
          >
            <section>
              <h2 class='onlineTitle'>Online</h2>
              <ul class='adventurerListContainer'>
                <For
                  each={
                    fetchedOnline()?.filteredByZone ||
                    fetchedOnline()?.filteredByJob ||
                    fetchedOnline()?.adventurersOnline
                  }
                >
                  {(adventurer) => (
                    <li class='adventurerRow'>
                      <Adventurer {...adventurer} />
                    </li>
                  )}
                </For>
              </ul>
            </section>
          </Show>
        </div>
      </div>
    </Suspense>
  )
}
