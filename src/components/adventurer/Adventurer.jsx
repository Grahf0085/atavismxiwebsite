import { createResource } from 'solid-js'
import HelpCircle from '../../../node_modules/lucide-solid/dist/source/icons/circle-help.jsx'
import { fetchAdventurerDetails } from '../../utils/adventurerDetails'
import '../../styles/adventurer/adventurer.css'

export default function Adventurer(props) {
  const [fetchedDetails] = createResource(
    props.charname,
    fetchAdventurerDetails,
  )

  return (
    <a
      class='detailsLink'
      href={`${
        props.charname
          ? `/adventurers/${props.charname}`
          : '/player/create-adventurer'
      }`}
    >
      <Show when={fetchedDetails()?.fetchedImage} fallback={<HelpCircle />}>
        <img
          class='advImage'
          src={fetchedDetails()?.fetchedImage}
          alt={props.charname}
        />
      </Show>

      <div class='advDetailsContainer'>
        <div class='nameContainer'>
          <b>{props.charname || 'Comer'}</b>
          <div class='titleSpan'>
            <em>the</em> {fetchedDetails()?.title || 'Future Adventurer'}
          </div>
        </div>
        <Show
          when={fetchedDetails()?.subJob}
          fallback={
            <span class='jobContainer'>
              {fetchedDetails()?.mainJob}
              {props.mlvl}
            </span>
          }
        >
          <div class='jobContainer'>
            {fetchedDetails()?.mainJob}
            {props.mlvl} / {fetchedDetails()?.subJob}
            {props.slvl}
          </div>
        </Show>
        <div class='adventurerLocation'>{fetchedDetails()?.translatedZone}</div>
      </div>
    </a>
  )
}
