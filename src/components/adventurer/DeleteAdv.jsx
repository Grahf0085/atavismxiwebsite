import { Show, createSignal } from 'solid-js'
import { createForm } from '@felte/solid'
import { navigate } from 'astro:transitions/client'
import '../../styles/adventurer/deleteAdv.css'

const baseUrl = import.meta.env.PUBLIC_BASEURL

export default function DeleteAdv(props) {
  const [killClicked, setKillClicked] = createSignal(false)
  const [nameConfirmation, setNameConfirmation] = createSignal('')

  const namesMatch = () => nameConfirmation() === props.charName

  const { form, errors } = createForm({
    onSubmit: async (values, { form }) => {
      const formData = new FormData(form)

      const response = await fetch(`${baseUrl}/api/adventurer/delete`, {
        /* can't get PATCH to work */
        method: 'POST',
        body: formData,
      })

      if (!response.ok) {
        throw await response.json()
      }

      return response
    },

    onError(err) {
      const errors = {}
      errors.serverErrors = err
      return errors
    },

    onSuccess() {
      const options = {
        history: 'replace',
      }

      navigate(`/player/${props.playerName}`, options)
    },
  })
  return (
    <div class='deleteContainer'>
      <button
        onClick={() => setKillClicked(!killClicked())}
        class='deleteButton'
        type='button'
      >
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='24'
          height='24'
          viewBox='0 0 24 24'
          fill='none'
          stroke='currentColor'
          stroke-width='2'
          stroke-linecap='round'
          stroke-linejoin='round'
          class='lucide lucide-heart-crack'
        >
          <path d='M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z' />
          <path d='m12 13-1-1 2-2-3-3 2-2' />
        </svg>
        Kill {props.charName}?
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='24'
          height='24'
          viewBox='0 0 24 24'
          fill='none'
          stroke='currentColor'
          stroke-width='2'
          stroke-linecap='round'
          stroke-linejoin='round'
          class='lucide lucide-heart-crack'
        >
          <path d='M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z' />
          <path d='m12 13-1-1 2-2-3-3 2-2' />
        </svg>
      </button>
      <Show when={killClicked()}>
        <label class='deleteNameLabel'>
          <span class='hidden'>
            Type the name of {props.charName} to Confirm You Want to Kill{' '}
            {props.charName}:
          </span>
          <input
            type='text'
            value={nameConfirmation()}
            onInput={(e) => setNameConfirmation(e.target.value)}
            placeholder={
              'Type adventurers name to confirm you want to delete him/her'
            }
          />
        </label>
      </Show>
      <Show when={namesMatch() && killClicked()}>
        <form use:form class='deleteForm'>
          <input type='hidden' name='accountId' value={props.charAccountId} />
          <input type='hidden' name='charId' value={props.charId} />
          <button
            class='deleteButton'
            type='submit'
            disabled={nameConfirmation() !== props.charName}
          >
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              stroke='currentColor'
              stroke-width='2'
              stroke-linecap='round'
              stroke-linejoin='round'
              class='lucide lucide-heart-crack'
            >
              <path d='M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z' />
              <path d='m12 13-1-1 2-2-3-3 2-2' />
            </svg>
            Kill {props.charName}!
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              stroke='currentColor'
              stroke-width='2'
              stroke-linecap='round'
              stroke-linejoin='round'
              class='lucide lucide-heart-crack'
            >
              <path d='M19 14c1.49-1.46 3-3.21 3-5.5A5.5 5.5 0 0 0 16.5 3c-1.76 0-3 .5-4.5 2-1.5-1.5-2.74-2-4.5-2A5.5 5.5 0 0 0 2 8.5c0 2.3 1.5 4.05 3 5.5l7 7Z' />
              <path d='m12 13-1-1 2-2-3-3 2-2' />
            </svg>
          </button>
          <Show when={errors().serverErrors?.errors?.charId}>
            <p>{errors().serverErrors?.errors?.charId}</p>
          </Show>
          <Show when={errors().serverErrors?.errors?.accountId}>
            <p>{errors().serverErrors?.errors?.accountId}</p>
          </Show>
          <Show when={errors().serverErrors?.errors}>
            <p>{errors().serverErrors?.errors}</p>
          </Show>
        </form>
      </Show>
    </div>
  )
}
