import { createSignal, For } from 'solid-js'
import { Select } from '@thisbeyond/solid-select'
import { rules } from '../utils/pageItems'
import '@thisbeyond/solid-select/style.css'
import '../styles/ruleList.css'

export default function RuleList(props) {
  const [selectedRules, setSelectedRules] = createSignal(rules.general)

  const options = [
    { value: JSON.stringify(rules.general), label: 'General' },
    { value: JSON.stringify(rules.ashitaApproved), label: 'Approved Ashita' },
    { value: JSON.stringify(rules.ashitaBanned), label: 'Banned Ashita' },
    {
      value: JSON.stringify(rules.windowerApproved),
      label: 'Approved Windower',
    },
    { value: JSON.stringify(rules.windowerBanned), label: 'Banned Windower' },
  ]

  const handleChange = (e) => {
    const selectedValue = e.value
    const selectedArray = JSON.parse(selectedValue)
    setSelectedRules(selectedArray)
  }

  return (
    <div class='rulesContainer'>
      <label for='ruleSelect' class='ruleLabel'>
        Select a Rule Set:
      </label>
      <Select
        onChange={(e) => handleChange(e)}
        class='custom'
        options={options}
        format={(item) => item.label}
        placeholder='General'
        id='ruleSelect'
      />
      <ol class='listContainer'>
        <For each={selectedRules()}>
          {(item) => <li class='rulesItem'>{item}</li>}
        </For>
      </ol>
    </div>
  )
}
