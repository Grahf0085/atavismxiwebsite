import { createSignal } from 'solid-js'
import FaceSelection from './FaceSelection'
import RaceSelection from './RaceSelection'
import SizeSelection from './SizeSelection'
import JobSelection from './JobSelection'
import NationSelection from './NationSelection'
import CreationForm from './CreationForm'
import Animate from './Animate'
import { races } from '../../utils/faces'
import '../../styles/createAdventurer/charCreation.css'

export default function AdventurerCreation(props) {
  const [step, setStep] = createSignal(0)
  const [selectedRace, setSelectedRace] = createSignal('hume')

  const facesToShow = () => races[selectedRace()]

  const [selectedFace, setSelectedFace] = createSignal(facesToShow()[0])
  const [selectedSize, setSelectedSize] = createSignal(0)
  const [selectedJob, setSelectedJob] = createSignal(1)
  const [selectedNation, setSelectedNation] = createSignal(0)

  return (
    <>
      <Animate
        showWhen={step() === 0}
        component={
          <RaceSelection
            selectedRace={selectedRace()}
            setSelectedRace={setSelectedRace}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 1}
        component={
          <FaceSelection
            selectedRace={selectedRace()}
            selectedFace={selectedFace()}
            setSelectedFace={setSelectedFace}
            facesToShow={facesToShow()}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 2}
        component={
          <SizeSelection
            selectedRace={selectedRace()}
            selectedFace={selectedFace().substring(0, 2)}
            selectedSize={selectedSize()}
            setSelectedSize={setSelectedSize}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 3}
        component={
          <JobSelection
            selectedJob={selectedJob()}
            setSelectedJob={setSelectedJob}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 4}
        component={
          <NationSelection
            selectedNation={selectedNation()}
            setSelectedNation={setSelectedNation}
            setStep={setStep}
          />
        }
      />

      <Animate
        showWhen={step() === 5}
        component={
          <CreationForm
            playerName={props.playerName}
            accountId={props.playerId}
            selectedRace={selectedRace()}
            selectedFace={selectedFace()}
            selectedSize={selectedSize()}
            selectedJob={selectedJob()}
            selectedNation={selectedNation()}
            setStep={setStep}
          />
        }
      />
    </>
  )
}
