import { createSignal } from 'solid-js'
import { navigate } from 'astro:transitions/client'
import { createForm } from '@felte/solid'
import '../styles/authForm.css'

const baseUrl = import.meta.env.PUBLIC_BASEURL

export default function Auth() {
  const [formType, setFormType] = createSignal('login')

  const { form, errors } = createForm({
    onSubmit: async (values, { event }) => {
      const formData = new FormData(event.target)

      if (formType() === 'login') {
        const loginResponse = await fetch(`${baseUrl}/api/auth/login`, {
          method: 'POST',
          body: formData,
        })

        if (!loginResponse.ok) {
          throw await loginResponse.json()
        }

        const loginData = await loginResponse.json()
        return loginData
      }

      if (formType() === 'register') {
        const registerResponse = await fetch(`${baseUrl}/api/auth/register`, {
          method: 'POST',
          body: formData,
        })

        if (!registerResponse.ok) {
          throw await registerResponse.json()
        }
        const registerData = await registerResponse.json()

        return registerData
      }
    },

    validate: async (values) => {
      const errors = {}

      if (!values.playerName)
        errors.playerName = [
          'Please enter a player name over three characters.',
        ]

      if (
        (values.playerName && values.playerName.length < 3) ||
        values.playerName.length > 16
      )
        errors.playerName = [
          'Player name must be between 3 and sixteen characters.',
        ]

      if (!values.password)
        errors.password = ['Please enter a password over five characters.']

      if (
        (values.password && values.password.length < 5) ||
        values.password.length > 64
      ) {
        errors.password = ['Password must be between five and 64 characters.']
      }

      if (formType() === 'register') {
        if (values.password !== values.confirmPassword)
          errors.passwordsMatch = ["Passwords don't match."]
      }
      return errors
    },

    onSuccess(response) {
      const options = {
        history: 'replace',
      }

      navigate(`/player/${response.name}`, options)
    },

    onError(err) {
      const errors = {}
      errors.serverErrors = err
      return errors
    },
  })

  return (
    <form use:form class='authForm'>
      <fieldset class='formTypeField'>
        <label
          class={`formType ${formType() === 'login' ? 'activeFormType' : ''}`}
        >
          Login
          <input
            type='radio'
            name='loginType'
            value='login'
            checked={formType() === 'login'}
            onChange={() => setFormType('login')}
            class='hidden'
          />
        </label>
        <label
          class={`formType ${
            formType() === 'register' ? 'activeFormType' : ''
          }`}
        >
          Register
          <input
            type='radio'
            name='loginType'
            value='register'
            checked={formType() === 'register'}
            onChange={() => setFormType('register')}
            class='hidden'
          />
        </label>
      </fieldset>
      <fieldset class='formInputsField'>
        <label>
          Player Name
          <input
            type='text'
            name='playerName'
            minlength={3}
            maxlength={16}
            required
          />
        </label>
        <Show when={errors()?.playerName}>{errors().playerName}</Show>
        <Show when={errors().serverErrors?.errors?.playerName}>
          <p>{errors().serverErrors?.errors?.playerName}</p>
        </Show>
        <label>
          Password
          <input
            type='password'
            name='password'
            minlength={5}
            maxlength={64}
            required
          />
        </label>
        <Show when={errors()?.password}>
          <p>{errors().password}</p>
        </Show>
        <Show when={errors().serverErrors?.errors?.password}>
          <p>{errors().serverErrors?.errors?.password}</p>
        </Show>
        <Show when={formType() === 'register'}>
          <label>
            Confirm Password
            <input
              type='password'
              name='confirmPassword'
              minlength={5}
              maxlength={64}
              required
            />
          </label>
        </Show>
        <Show when={errors()?.passwordsMatch}>
          <p>{errors().passwordsMatch}</p>
        </Show>
        <button class='formSubmit'>Submit</button>
        <Show when={errors().serverErrors?.message}>
          <p>{errors().serverErrors.message}</p>
        </Show>
        <Show when={errors().serverErrors?.errors}>
          <p>{errors().serverErrors?.errors}</p>
        </Show>
        <Show when={errors().serverErrors}>
          <p>{errors().serverErrors}</p>
        </Show>
      </fieldset>
    </form>
  )
}
