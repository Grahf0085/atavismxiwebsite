export const prerender = false

import { deleteAdventurer } from '../../../server/adventurerAuth.js'
import { getPlayer } from '../../../utils/auth.js'
import { getHeaders } from '../../../utils/headers.js'

/* can't get PATCH to work */
export const POST = async ({ request, cookies }) => {
  const origin = request.headers.get('origin')
  const headers = getHeaders(origin)

  const data = await request.formData()
  const accountId = Number(data.get('accountId'))
  const charId = Number(data.get('charId'))

  const errors = { charId: '', accountId: '' }

  if (isNaN(accountId) || accountId < 1000 || accountId > 4294967295) {
    errors.accountId += 'Account id must be a number greater than 999.'
  }

  if (isNaN(charId) || charId < 1 || charId > 4294967295) {
    errors.charId += 'Character id must be a number greater than one.'
  }

  await getPlayer(cookies)
    .then((player) => {
      if (Number(player?.id) !== Number(accountId)) {
        errors.accountId +=
          "You are trying to delete an adventurer in another's account. N00b!"
      }
    })
    .catch((error) => {
      console.error('Get Player During Deletion Failed: ', error)
      errors.accountId += error
    })

  const hasErrors = Object.values(errors).some((msg) => msg)

  if (hasErrors) {
    console.error(errors)

    return new Response(
      JSON.stringify({
        message: 'Adventurer Deletion Form Failed',
        errors: errors,
      }),
      {
        status: 400,
        headers: headers,
      },
    )
  }

  try {
    await deleteAdventurer(accountId, charId)

    return new Response(
      {
        message: 'Deleted Adventurer Successfully',
      },
      {
        status: 200,
        headers: headers,
      },
    )
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Failed to Delete Adventurer',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
