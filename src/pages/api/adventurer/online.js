export const prerender = false

import { findAllOnline } from '../../../server/adventurer.js'
import { getHeaders } from '../../../utils/headers.js'

export async function GET({ request }) {
  try {
    const origin = request.headers.get('origin')
    const headers = getHeaders(origin)

    const onlineAdventurers = await findAllOnline()

    if (!onlineAdventurers) {
      return new Response(null, {
        status: 404,
        statusText: 'Not found',
        headers: headers,
      })
    }

    return new Response(JSON.stringify(onlineAdventurers), {
      status: 200,
      headers: headers,
    })
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Fetching Online Adventurers Failed',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
