export const prerender = false

import { findEquipment } from '../../../../server/equipment.js'
import { getHeaders } from '../../../../utils/headers.js'

export async function GET({ params, request }) {
  const name = params.name

  try {
    const results = await findEquipment(name)
    const origin = request.headers.get('origin')
    const headers = getHeaders(origin)

    if (!results) {
      return new Response(null, {
        status: 404,
        statusText: 'Not found',
        headers: headers,
      })
    }

    return new Response(JSON.stringify(results), {
      status: 200,
      headers: headers,
    })
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Fetching Equipment By Name Failed',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
