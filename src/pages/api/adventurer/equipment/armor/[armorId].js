export const prerender = false

import { findArmorDetails } from '../../../../../server/equipment.js'
import { getHeaders } from '../../../../../utils/headers.js'

export async function GET({ params, request }) {
  const armorId = params.armorId
  const results = await findArmorDetails(armorId)
  const origin = request.headers.get('origin')
  const headers = getHeaders(origin)

  if (!results) {
    return new Response(null, {
      status: 404,
      statusText: 'Not found',
      headers: headers,
    })
  }

  return new Response(JSON.stringify(results), {
    status: 200,
    headers: headers,
  })
}
