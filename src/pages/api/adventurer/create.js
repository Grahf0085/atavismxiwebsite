export const prerender = false

import {
  findAdventurer,
  createAdventurer,
} from '../../../server/adventurerAuth'
import { getPlayer } from '../../../utils/auth.js'
import { pickStartingZone } from '../../../utils/createAdventurer'
import { getHeaders } from '../../../utils/headers.js'

export const POST = async ({ request, cookies }) => {
  const origin = request.headers.get('origin')
  const headers = getHeaders(origin)

  const data = await request.formData()
  const adventurerName = data.get('adventurerName')
  const accountId = data.get('accountId')
  const face = data.get('face')
  const race = data.get('race')
  const size = data.get('size')
  const job = data.get('job')
  const nation = data.get('nation')

  const errors = {
    adventurerName: '',
    accountId: '',
    face: '',
    race: '',
    size: '',
    job: '',
    nation: '',
  }

  if (
    typeof adventurerName !== 'string' ||
    adventurerName.length < 3 ||
    adventurerName.length > 15
  ) {
    errors.adventurerName +=
      'Please enter an adventurer name between three and 15 characters.'
  }

  if (adventurerName.charAt(0) !== adventurerName.charAt(0).toUpperCase()) {
    errors.adventurerName +=
      'First letter of adventurer name must be capitalized.'
  }

  if (
    adventurerName.substring(1) !== adventurerName.substring(1).toLowerCase()
  ) {
    errors.adventurerName +=
      'All letters besides the first letter need to be lowercase.'
  }

  if (!/^[a-zA-Z]+$/.test(adventurerName)) {
    errors.adventurerName += 'Adventurer names can only contain letters.'
  }

  await getPlayer(cookies)
    .then((player) => {
      if (Number(player?.id) !== Number(accountId)) {
        errors.accountId +=
          "You are trying to create an adventurer in another's account. N00b!"
      }
    })
    .catch((error) => {
      console.error('Get Player During Creation Failed: ', error)
      errors.accountId += error
    })

  await findAdventurer(adventurerName)
    .then((adventurerNameTaken) => {
      if (adventurerNameTaken.length >= 1) {
        errors.adventurerName += `Adventurer name ${adventurerName} is already taken.`
      }
    })
    .catch((error) => {
      console.error('Find Adventurer During Creation Failed: ', error)
      errors.adventurerName += error
    })

  if (isNaN(accountId) || accountId < 1000 || accountId > 4294967295) {
    errors.accountId += 'Account id must be a number greater than 999.'
  }

  if (isNaN(face) || face > 15 || face < 0) {
    errors.face += 'Face must be a number between 0 and fifteen.'
  }

  if (isNaN(race) || race < 1 || race > 8) {
    errors.race += 'Race must be a number between 1 and 8.'
  }

  if (isNaN(size) || size > 2 || size < 0) {
    errors.size += 'Size must be a number between 0 and 2.'
  }

  if (isNaN(job) || job < 1 || job > 6) {
    errors.job += 'Job must be a number between 1 and 6.'
  }

  if (isNaN(nation) || nation < 0 || nation > 2) {
    errors.nation += 'Nation must be a number between 0 and 2.'
  }

  const hasErrors = Object.values(errors).some((msg) => msg)

  if (hasErrors) {
    console.debug(errors)

    return new Response(
      JSON.stringify({
        message: 'Adventurer Creation Form Failed',
        errors: errors,
      }),
      {
        status: 400,
        headers: headers,
      },
    )
  }

  try {
    const posZone = pickStartingZone(parseInt(nation))

    await createAdventurer(
      accountId,
      adventurerName,
      posZone,
      nation,
      face,
      race,
      size,
      job,
    )

    return new Response(
      {
        message: 'Created Adventurer Successfully',
      },
      {
        status: 200,
        headers: headers,
      },
    )
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Adventure Creation Failed',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
