export const prerender = false

import { findRecipesByCraft } from '../../../server/synthesis'
import { getHeaders } from '../../../utils/headers.js'

export async function GET({ params, request }) {
  const craft = params.craft
  const origin = request.headers.get('origin')
  const headers = getHeaders(origin)

  try {
    const recipes = await findRecipesByCraft(craft)

    if (!recipes) {
      return new Response(null, {
        status: 404,
        statusText: 'Not found',
        headers: headers,
      })
    }

    return new Response(JSON.stringify(recipes), {
      status: 200,
      headers: headers,
    })
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Fetching Crafts Failed',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
