export const prerender = false

import { getHeaders } from '../../../utils/headers'

export async function GET({ params, request }) {
  const clientVersion = params.version
  const origin = request.headers.get('origin')
  const headers = getHeaders(origin)

  const availableVersions = ['1.0']

  const newerVersions = availableVersions.filter(
    (version) => version > clientVersion,
  )

  if (newerVersions.length > 0) {
    const downloadLinks = newerVersions.map((version) => ({
      version,
      link: `https://www.atavismxi.com/update/AtavismXI-${version}.zip`,
    }))

    return new Response(JSON.stringify(downloadLinks), {
      status: 200,
      headers: headers,
    })
  } else {
    return new Response(null, {
      status: 204,
      headers: headers,
    })
  }
}
