export const prerender = false

import { getPlayer, getPlayerSession } from '../../../utils/auth'
import { getHeaders } from '../../../utils/headers'

export async function GET({ cookies, request }) {
  try {
    const origin = request.headers.get('origin')
    const headers = getHeaders(origin)

    const session = getPlayerSession(cookies)

    if (!session) {
      return new Response(null, {
        status: 204,
        headers: headers,
      })
    }
    const results = await getPlayer(cookies)

    if (!results) {
      return new Response(JSON.stringify(null), {
        status: 404,
        message: 'Not found',
        headers: headers,
      })
    }

    return new Response(JSON.stringify(results), {
      status: 200,
      headers: headers,
    })
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Fetching Player Info Failed',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
