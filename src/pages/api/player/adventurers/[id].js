export const prerender = false

import { findPlayersAdventurers } from '../../../../server/player.js'
import { getHeaders } from '../../../../utils/headers.js'

export async function GET({ params, request }) {
  const origin = request.headers.get('origin')
  const headers = getHeaders(origin)

  const playerId = params.id

  try {
    const allPlayerAdventurers = await findPlayersAdventurers(playerId)

    if (!allPlayerAdventurers) {
      return new Response(null, {
        status: 404,
        statusText: 'Not found',
        headers: headers,
      })
    }

    return new Response(JSON.stringify(allPlayerAdventurers), {
      status: 200,
      headers: headers,
    })
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Fetching Players Adventurers Failed',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
