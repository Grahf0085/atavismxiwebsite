export const prerender = false

import { createPlayer, findPlayer } from '../../../server/playerAuth'
import { createToken } from '../../../utils/auth'
import { getHeaders } from '../../../utils/headers'

export const POST = async (ctx, request) => {
  const origin = ctx.request.headers.get('origin')
  const headers = getHeaders(origin)

  const data = await request.formData()
  const playerName = data.get('playerName')
  const password = data.get('password')

  const errors = { playerName: '', password: '' }

  if (
    typeof playerName !== 'string' ||
    playerName.length < 3 ||
    playerName.length > 16
  ) {
    errors.playerName +=
      'Please enter a player name between three and 16 characters.'
  }
  if (
    typeof password !== 'string' ||
    password.length < 5 ||
    password.length > 64
  ) {
    errors.password += 'Please enter a password between five and 64 characters.'
  }

  await findPlayer({ where: { playerName } })
    .then((playerAlreadyExists) => {
      if (playerAlreadyExists) {
        errors.playerName += `Player name ${playerName} is already taken.`
      }
    })
    .catch((error) => {
      console.error('Finding Player During Regisration Failed', error)
      errors.playerName += error
    })

  const hasErrors = Object.values(errors).some((msg) => msg)

  if (hasErrors) {
    console.error(errors)

    return new Response(
      JSON.stringify({
        message: 'Registration Form Failed',
        errors: errors,
      }),
      {
        status: 400,
        headers: headers,
      },
    )
  }

  try {
    const player = await createPlayer(playerName, password)

    if (!player) {
      return new Response(
        JSON.stringify({
          message: 'Create Player Failed',
        }),
        {
          status: 500,
          headers: headers,
        },
      )
    }

    const token = createToken(ctx, player.id)

    if (!token) {
      return new Response(
        JSON.stringify({
          message: 'Failed to create authentication token',
        }),
        {
          status: 500,
          headers: headers,
        },
      )
    }

    return new Response(
      JSON.stringify({
        message: 'Registered Successfully',
        name: player.login,
        id: player.id,
      }),
      {
        status: 200,
        headers: headers,
      },
    )
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Registration Failed',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
