export const prerender = false

import { getHeaders } from '../../../utils/headers'

const TOKEN = import.meta.env.TOKEN

export const POST = async (ctx) => {
  try {
    const origin = ctx.request.headers.get('origin')
    const headers = getHeaders(origin)

    ctx.cookies.set(TOKEN, '', {
      httpOnly: true,
      maxAge: 0,
      path: '/',
      secure: true,
      sameSite: 'none',
      domain: '.atavismxi.com',
    })

    return new Response(
      {
        message: "You're logged out!",
      },
      {
        status: 200,
        headers: headers,
      },
    )
  } catch (error) {
    console.error(error)
    return new Response(
      JSON.stringify({
        message: 'Logout Failed',
        errors: error,
      }),
      {
        status: 500,
        headers: headers,
      },
    )
  }
}
