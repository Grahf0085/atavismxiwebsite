import { defineConfig } from 'astro/config'
import solidJs from '@astrojs/solid-js'
import node from '@astrojs/node'

const isDev = import.meta.env.DEV

export default defineConfig({
  site: 'https://www.atavismxi.com',
  server: {
    port: isDev ? 2007 : 2005,
  },
  integrations: [solidJs()],
  output: 'hybrid',
  adapter: node({
    mode: 'standalone',
  }),
})
